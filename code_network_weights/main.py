# Multi-temporal MERLIN, Copyright (C) 2022, Ines Meraoumia, Emanuele Dalsasso, Loic Denis, Remy Abergel, Florence Tupin
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of the License.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, see https://gitlab.telecom-paris.fr/ring/multi-temporal-merlin



import argparse

import tensorflow as tf
import numpy as np
from glob import glob
import os

from model import denoiser
from utils import *

tf.set_random_seed(0)
np.random.seed(0)


parser = argparse.ArgumentParser(description='')




parser.add_argument('--basedir', dest='basedir', type=str, default='.', help='base dir')

parser.add_argument('--input_c', dest='input_c', type=int, default=3, help='# of input channels')

parser.add_argument('--stride_size', dest='stride_size', type=int, default=64, help='# size of the stride')

parser.add_argument('--use_gpu', dest='use_gpu', type=int, default=1, help='gpu flag, 1 for GPU and 0 for CPU')


args = parser.parse_args()



#directory containing the test data, images to denoise
test_set = args.basedir + "test_data/"

#directory containing the denoised estimation on test images
test_dir = args.basedir + "results/"

#directory containing the weights of the Multi-temporal MERLIN networks
#different number of inputs are available: 2, 3, 4, 5, 10, 16, 20
weights_dir = args.basedir + "weights/"



def denoiser_test(denoiser):
    test_files = glob(test_set + '*.npy')
    denoiser.test(test_files, test_set, weights_dir, save_dir=test_dir, input_c=args.input_c, stride=args.stride_size)

    
    
    

def main(_):
    
    if args.use_gpu:
        # added to control the gpu memory
        print("GPU\n")
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        with tf.Session(config=config) as sess:
            model = denoiser(sess, input_c=args.input_c, stride=args.stride_size)
            
            denoiser_test(model)
            
           
    else:
        print("CPU\n")
        with tf.Session() as sess:
            model = denoiser(sess, input_c=args.input_c, stride=args.stride_size)
            
            denoiser_test(model)
            


if __name__ == '__main__':
    tf.app.run()
