# Multi-temporal MERLIN, Copyright (C) 2022, Ines Meraoumia, Emanuele Dalsasso, Loic Denis, Remy Abergel, Florence Tupin
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of the License.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, see https://gitlab.telecom-paris.fr/ring/multi-temporal-merlin



import tensorflow as tf





#up-sampling function by a factor 2
def upscale2d(x, factor=2):
    assert isinstance(factor, int) and factor >= 1
    if factor == 1:
        return x
    with tf.variable_scope('Upscale2D'):
        s = x.shape
        x = tf.reshape(x, [-1, s[1], 1, s[2], 1, s[3]])
        x = tf.tile(x, [1, 1, factor, 1, factor, 1])
        x = tf.reshape(x, [-1, s[1] * factor, s[2] * factor, s[3]])
        return x

    
    
    
    
    
    
    
    
    
regularizer = tf.contrib.layers.l2_regularizer(0.1)


#unet architecture, residual mode
def autoencoder(x, input_c, width=256, height=256, **_kwargs):

    x.set_shape([None, height, width, input_c]) 

    skips = [x]

    n = x
    n = tf.nn.leaky_relu(tf.layers.conv2d(n, 48, 3, padding='same', name='enc_conv0', kernel_regularizer=regularizer), alpha=0.1)
    n = tf.nn.leaky_relu(tf.layers.conv2d(n, 48, 3, padding='same', name='enc_conv1', kernel_regularizer=regularizer), alpha=0.1)
    n = tf.nn.max_pool(n, ksize=[1,2,2,1], strides=[1,2,2,1], padding='SAME')
    skips.append(n)

    n = tf.nn.leaky_relu(tf.layers.conv2d(n, 48, 3, padding='same', name='enc_conv2', kernel_regularizer=regularizer), alpha=0.1)
    n = tf.nn.max_pool(n, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
    skips.append(n)

    n = tf.nn.leaky_relu(tf.layers.conv2d(n, 48, 3, padding='same', name='enc_conv3', kernel_regularizer=regularizer), alpha=0.1)
    n = tf.nn.max_pool(n, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
    skips.append(n)

    n = tf.nn.leaky_relu(tf.layers.conv2d(n, 48, 3, padding='same', name='enc_conv4', kernel_regularizer=regularizer), alpha=0.1)
    n = tf.nn.max_pool(n, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
    skips.append(n)

    n = tf.nn.leaky_relu(tf.layers.conv2d(n, 48, 3, padding='same', name='enc_conv5', kernel_regularizer=regularizer), alpha=0.1)
    n = tf.nn.max_pool(n, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
    n = tf.nn.leaky_relu(tf.layers.conv2d(n, 48, 3, padding='same', name='enc_conv6', kernel_regularizer=regularizer), alpha=0.1)


    # -----------------------------------------------
    n = upscale2d(n)
    n = tf.concat([n, skips.pop()], axis=-1)
    n = tf.nn.leaky_relu(tf.layers.conv2d(n, 96, 3, padding='same', name='dec_conv5', kernel_regularizer=regularizer), alpha=0.1)
    n = tf.nn.leaky_relu(tf.layers.conv2d(n, 96, 3, padding='same', name='dec_conv5b', kernel_regularizer=regularizer), alpha=0.1)

    n = upscale2d(n)
    n = tf.concat([n, skips.pop()], axis=-1)
    n = tf.nn.leaky_relu(tf.layers.conv2d(n, 96, 3, padding='same', name='dec_conv4', kernel_regularizer=regularizer), alpha=0.1)
    n = tf.nn.leaky_relu(tf.layers.conv2d(n, 96, 3, padding='same', name='dec_conv4b', kernel_regularizer=regularizer), alpha=0.1)

    n = upscale2d(n)
    n = tf.concat([n, skips.pop()], axis=-1)
    n = tf.nn.leaky_relu(tf.layers.conv2d(n, 96, 3, padding='same', name='dec_conv3', kernel_regularizer=regularizer), alpha=0.1)
    n = tf.nn.leaky_relu(tf.layers.conv2d(n, 96, 3, padding='same', name='dec_conv3b', kernel_regularizer=regularizer), alpha=0.1)

    n = upscale2d(n)
    n = tf.concat([n, skips.pop()], axis=-1)
    n = tf.nn.leaky_relu(tf.layers.conv2d(n, 96, 3, padding='same', name='dec_conv2', kernel_regularizer=regularizer), alpha=0.1)
    n = tf.nn.leaky_relu(tf.layers.conv2d(n, 96, 3, padding='same', name='dec_conv2b', kernel_regularizer=regularizer), alpha=0.1)

    n = upscale2d(n)
    n = tf.concat([n, skips.pop()], axis=-1)
    n = tf.nn.leaky_relu(tf.layers.conv2d(n, 64, 3, padding='same', name='dec_conv1a', kernel_regularizer=regularizer), alpha=0.1)
    n = tf.nn.leaky_relu(tf.layers.conv2d(n, 32, 3, padding='same', name='dec_conv1b', kernel_regularizer=regularizer), alpha=0.1)

    n = tf.layers.conv2d(n, 1, 3, padding='same', name='dec_conv1', kernel_regularizer=regularizer)

    
    
    
    return tf.expand_dims(x[:,:,:,0], 3) - n
