# Multi-temporal MERLIN, Copyright (C) 2022, Ines Meraoumia, Emanuele Dalsasso, Loic Denis, Remy Abergel, Florence Tupin
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of the License.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, see https://gitlab.telecom-paris.fr/ring/multi-temporal-merlin


import numpy as np
from scipy import special
import os
import itertools
import random

#python files needed 
from utils import *
from u_net import *



# Parameters needed for the normalization of the input images
M = 10.089038980848645
m = -1.429329123112601
L = 1
c = (1 / 2) * (special.psi(L) - np.log(L))
cn = c / (M - m)  # mean of the log speckle (images scaled in [0,1] )









class denoiser(object):
    def __init__(self, sess, input_c, stride=128, batch_size=4):
        
        self.sess = sess
        self.input_c_dim = input_c
        
        # build model
        self.Y_input = tf.placeholder(tf.float32, [None, None, None, self.input_c_dim],
                                 name='input_stack')
        self.Y_real = tf.placeholder(tf.float32, [None, None, None, 1], 
                                 name='imag_image')
        self.Y_imag = tf.placeholder(tf.float32, [None, None, None, 1], 
                                 name='imag_image')
      


        self.Y = autoencoder((self.Y_input), input_c=self.input_c_dim) # denoised image

        
        
        # ----- loss -----
        self.log_hat_R = 2*(self.Y*(M-m)+m)
        self.hat_R = tf.exp(self.log_hat_R)+1e-6 
        
        
        self.b_square = tf.square(self.Y_imag)
        self.loss = (1.0 / (batch_size)) * tf.math.reduce_mean( 0.5*self.log_hat_R+self.b_square/self.hat_R  ) 

        self.lr = tf.placeholder(tf.float32, name='learning_rate')
        optimizer = tf.train.AdamOptimizer(self.lr, name='AdamOptimizer')
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        
        with tf.control_dependencies(update_ops):
            # added control on gradient to avoid explosion
            gradients, variables = zip(*optimizer.compute_gradients(self.loss))
            for gradient in gradients: self.print_op = tf.print(tf.norm(gradient),output_stream=sys.stderr)
            gradients = [
                None if gradient is None else tf.clip_by_norm(gradient,5.0)
                for gradient in gradients]


        init = tf.global_variables_initializer()
        self.sess.run(init)
        print("[*] The network is correctly initialized.")
        
        
      
        
   
        
        

    
    def load(self, checkpoint_dir, input_c):
        print("[*] Loading weights...")
        ckpt_filename = checkpoint_dir+"/checkpoint_"+str(input_c)
        saver = tf.train.Saver()
        ckpt = tf.train.get_checkpoint_state(checkpoint_dir, latest_filename=ckpt_filename)
        print(ckpt)
        if ckpt and ckpt.model_checkpoint_path:
            full_path = tf.train.latest_checkpoint(checkpoint_dir, latest_filename=ckpt_filename)
            saver.restore(self.sess, full_path)
            return True
        else:
            return False, 0
        
        
        
        
        
        

    def test(self, test_files, test_set, ckpt_dir, save_dir, input_c, stride):
        
        tf.initialize_all_variables().run()
        assert len(test_files) != 0, 'No testing data!'
        load_model_status = self.load(ckpt_dir, input_c)
        assert load_model_status == True, '[!] Load weights FAILED...'
        print(" [*] The weights are correctly loaded...")
        print("[*] Start testing...")
        for idx in range(len(test_files)):
            real_image = load_sar_images(test_files[idx], input_c).astype(np.float32)  # range [0; 1]
            current_test = np.copy(real_image)
            
            image_real_part = current_test[:, :, :, 0, 0]
                                                                 
            image_imag_part = current_test[:, :, :, 1, 0]
                                                                 

            test_im_real = np.zeros((1, current_test.shape[1], current_test.shape[2], input_c))
            test_im_im = np.zeros((1, current_test.shape[1], current_test.shape[2], input_c))
            
            #log-normalization
            test_im_real[:,:,:,0] = (np.log(np.square(image_real_part)+1e-3)+np.log(2)-2*m)/(2*(M-m))
            test_im_im[:,:,:,0] = (np.log(np.square(image_imag_part)+1e-3)+np.log(2)-2*m)/(2*(M-m))
            
            
            for l in range(1,input_c):
                test_im_real[:,:,:,l] = normalize_sar(np.abs(current_test[:, :, :, 0, l]+1j*current_test[:, :, :, 1, l]))
                test_im_im[:,:,:,l] = normalize_sar(np.abs(current_test[:, :, :, 0, l]+1j*current_test[:, :, :, 1, l]))

                
            image_real_part = image_real_part.reshape((image_real_part.shape[0], image_real_part.shape[1], image_real_part.shape[2], 1))
            image_imag_part = image_imag_part.reshape((image_imag_part.shape[0], image_imag_part.shape[1], image_imag_part.shape[2], 1))
            
            

            #padding of the image with 256x256 patches
            pat_size = 256
            im_h = np.size(real_image, 1)
            im_w = np.size(real_image, 2)

            count_image = np.zeros(image_imag_part.shape)
             
            output_clean_image_1 = np.zeros((image_real_part.shape[0], image_real_part.shape[1], image_real_part.shape[2], 1))
            output_clean_image_2 = np.zeros((image_real_part.shape[0], image_real_part.shape[1], image_real_part.shape[2], 1))
            
            
            if im_h == pat_size:
                x_range = list(np.array([0]))
            else:
                x_range = list(range(0, im_h - pat_size, stride))
                if (x_range[-1] + pat_size) < im_h: x_range.extend(range(im_h - pat_size, im_h - pat_size + 1))

            if im_w == pat_size:
                y_range = list(np.array([0]))
            else:
                y_range = list(range(0, im_w - pat_size, stride))
                if (y_range[-1] + pat_size) < im_w: y_range.extend(range(im_w - pat_size, im_w - pat_size + 1))
            
            
            #denoising each patch and agregate the results by averaging on each pixel
            for x in x_range:
                for y in y_range:
                    
                    tmp_clean_image_real = self.sess.run([self.Y],
                                                         feed_dict={self.Y_input: test_im_real[:, x:x + pat_size,
                                                                                 y:y + pat_size, :],
                                                                    self.Y_real: image_real_part[:, x:x + pat_size, 
                                                                                                 y:y + pat_size, :]})
                    
                    
                    
                    output_clean_image_1[:, x:x + pat_size, y:y + pat_size, :] = output_clean_image_1[:, x:x + pat_size,
                                                                                 y:y + pat_size,
                                                                                 :] + tmp_clean_image_real
                   
                    
                    tmp_clean_image_imag = self.sess.run([self.Y],
                                                         feed_dict={self.Y_input: test_im_im[:, x:x + pat_size,
                                                                                 y:y + pat_size, :],
                                                                    self.Y_real: image_imag_part[:, x:x + pat_size, 
                                                                                                 y:y + pat_size, :]})
                    
                    
                    
                    output_clean_image_2[:, x:x + pat_size, y:y + pat_size, :] = output_clean_image_2[:, x:x + pat_size,
                                                                                 y:y + pat_size,
                                                                                 :] + tmp_clean_image_imag
                    count_image[:, x:x + pat_size, y:y + pat_size, :] = count_image[:, x:x + pat_size, y:y + pat_size,
                                                                        :] + np.ones((1, pat_size, pat_size, 1))
                    
            
            
            #final estimation        
            output_clean_image_1 = output_clean_image_1 / count_image
            output_clean_image_2 = output_clean_image_2 / count_image
            
            #averaging the denoised reflectivities estimated based on the real and imaginary parts
            output_clean_image = 0.5 * (np.square(denormalize_sar(output_clean_image_1)) + np.square(
                denormalize_sar(output_clean_image_2)))
            
            noisyimage = np.squeeze(np.sqrt(image_real_part ** 2 + image_imag_part ** 2))
            #saving the result as an amplitude image
            outputimage = np.sqrt(np.squeeze(output_clean_image))
            imagename = test_files[idx].replace(test_set, "")
            print("denoised " + imagename)
            save_sar_images(outputimage, noisyimage, imagename, save_dir)
            

        print("--- End of testing ---")

