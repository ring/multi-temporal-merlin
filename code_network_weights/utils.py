# Multi-temporal MERLIN, Copyright (C) 2022, Ines Meraoumia, Emanuele Dalsasso, Loic Denis, Remy Abergel, Florence Tupin
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of the License.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details, see https://gitlab.telecom-paris.fr/ring/multi-temporal-merlin



import gc
import os
import sys



import numpy as np
from PIL import Image
from scipy import special, signal
import matplotlib.pyplot as plt
from glob import glob
import random




# Parameters needed for the normalization of the input images
M = 10.089038980848645
m = -1.429329123112601
L = 1
c = (1 / 2) * (special.psi(L) - np.log(L))
cn = c / (M - m)  # mean of the log speckle (images scaled in [0,1] )




#normalizing AMPLITUDE images
def normalize_sar(im):
    return ((np.log(im+1e-12)-m)/(M-m)).astype(np.float32)

def denormalize_sar(im):
    return np.exp((np.clip(np.squeeze(im),0,1))*(M-m)+m)






# Zero-doppler centering for the reference spectrum
# Translation of the power spectrum for the rest of the stack
def symetrisation_im_ref(ima):
        S = np.fft.fftshift(np.fft.fft2(ima[:,:,0]+1j*ima[:,:,1]))
        p = np.zeros((S.shape[0]))
        for i in range(S.shape[0]):
            p[i] = np.mean(np.abs(S[i,:]))
        sp = p[::-1]
        c = np.real(np.fft.ifft(np.fft.fft(p)*np.conjugate(np.fft.fft(sp))))
        d1 = np.unravel_index(c.argmax(),p.shape[0])
        d1 = d1[0]
        shift_az_1 = int(round(-(d1-1)/2))%p.shape[0]+int(p.shape[0]/2)
        p2_1 = np.roll(p,shift_az_1)
        shift_az_2 = int(round(-(d1-1-p.shape[0])/2))%p.shape[0]+int(p.shape[0]/2)
        p2_2 = np.roll(p,shift_az_2)
        window = signal.gaussian(p.shape[0], std=0.2*p.shape[0])
        test_1 = np.sum(window*p2_1)
        test_2 = np.sum(window*p2_2)
        if test_1>=test_2:
            p2 = p2_1
            shift_az = shift_az_1/p.shape[0]
        else:
            p2 = p2_2
            shift_az = shift_az_2/p.shape[0]
        S2 = np.roll(S,int(shift_az*p.shape[0]),axis=0)
        ima2 = np.fft.ifft2(np.fft.ifftshift(S2))
        return np.stack((np.real(ima2),np.imag(ima2)),axis=2), int(shift_az*p.shape[0])
    
    
    
def symetrisation_others(ima, shift):
        S = np.fft.fftshift(np.fft.fft2(ima[:,:,0]+1j*ima[:,:,1]))
        
        S2 = np.roll(S,shift,axis=0)
       
        ima2 = np.fft.ifft2(np.fft.ifftshift(S2))
        return np.stack((np.real(ima2),np.imag(ima2)),axis=2)    
    
    
    
    
def symetrisation_globale(im_pile, index_ref):
    im_sym = np.zeros(im_pile.shape)
    im_sym[:,:,:,index_ref], shift = symetrisation_im_ref(im_pile[:,:,:,index_ref])
    for i in range(im_pile.shape[-1]):
        if i!=index_ref:
            im_sym[:,:,:,i] = symetrisation_others(im_pile[:,:,:,i], shift)
    return im_sym    
    
    



def load_sar_images(filelist, input_c):
    if not isinstance(filelist, list):
        im_to_recal = np.load(filelist)
        im = np.zeros(im_to_recal.shape)
        im = symetrisation_globale(im_to_recal, 0)
        return np.array(im).reshape(1, np.size(im, 0), np.size(im, 1), 2, input_c)
    
    data = []
    for file in filelist:
        im_to_recal = np.load(file)
        im = np.zeros(im_to_recal.shape)
        im = symetrisation_globale(im_to_recal, 0)
        data.append(np.array(im).reshape(1, np.size(im, 0), np.size(im, 1), 2, input_c))
    return data





def store_data_and_plot(im, threshold, filename):
    im = np.clip(im, 0, threshold)
    im = im / threshold * 255
    im = Image.fromarray(im.astype('float64')).convert('L')
    im.save(filename.replace('npy','png'))


    
def save_sar_images(denoised, noisy, imagename, save_dir, threshold=None):
    #default value for the threshold to display and save .png images
    if not threshold:
        threshold = np.mean(noisy) + 3 * np.std(noisy)
    
    #save noisy image
    noisyfilename = save_dir + "noisy_" + imagename
    np.save(noisyfilename, noisy)
    store_data_and_plot(noisy, threshold, noisyfilename)
    
    #save denoised image
    denoisedfilename = save_dir + "denoised_" + imagename
    np.save(denoisedfilename, denoised)
    store_data_and_plot(denoised, threshold, denoisedfilename)



