# Multi-temporal speckle reduction with self-supervised deep neural networks
## Inès Meraoumia, Emanuele Dalsasso, Loïc Denis, Rémy Abergel, Florence Tupin



## Abstract
Speckle filtering is generally a prerequisite to the analysis of synthetic aperture radar (SAR) images. Tremendous progress has been achieved in the domain of single-image despeckling. Latest techniques rely on deep neural networks to restore the various structures and textures peculiar to SAR images. The availability of time series of SAR images offers the possibility of improving speckle filtering by combining different speckle realizations over the same area.

The supervised training of deep neural networks requires ground-truth speckle-free images. Such images can only be obtained indirectly through some form of averaging, by spatial or temporal integration, and are imperfect. Given the potential of very high quality restoration reachable by multi-temporal speckle filtering, the limitations of ground-truth images need to be circumvented. We extend a recent self-supervised training strategy for single-look complex SAR images, called MERLIN, to the case of multi-temporal filtering. This requires modeling the sources of statistical dependencies in the spatial and temporal dimensions as well as between the real and imaginary components of the complex amplitudes.

Quantitative analysis on datasets with simulated speckle indicates a clear improvement of speckle reduction when additional SAR images are included. Our method is then applied to stacks of TerraSAR-X images and shown to outperform competing multi-temporal speckle filtering approaches.



## Resources
- [Preprint on ArXiv](https://arxiv.org/) TODO


To cite the article:
```
I. Meraoumia, E. Dalsasso, L. Denis and F. Tupin,
"Multi-temporal speckle reduction with self-supervised deep neural networks,"
...

```

## Code

A notebook is available on the [GitLab repositary](https://gitlab.telecom-paris.fr/ring/multi-temporal-merlin/) to test the code on a TerraSAR-X test set. Further instructions about the format of the input data are to be found in the notebook.

## Licence

The material is made available under the GNU General Public License v3.0, Copyright (C) 2022, Ines Meraoumia, Emanuele Dalsasso, Loic Denis, Remy Abergel, Florence Tupin





